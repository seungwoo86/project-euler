#If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.
#Find the sum of all the multiples of 3 or 5 below 1000.

def isMultipleOf3or5(a):
	if (a % 3 == 0 or a % 5 == 0):
		return True
	else:
		return False

sum_of_all_the_multiples = 0
for i in range(1,1000):
	if(isMultipleOf3or5(i)):
		sum_of_all_the_multiples += i

print sum_of_all_the_multiples
raw_input()

